<style type="text/css">
	/* NAVI */
	nav{
		background-color: #f4f4f4;
		border-bottom: 1px solid #e2d5d5;
		height: 50px;
	}
	nav ul {
		list-style-type: none;
		padding: 0;
		overflow: hidden;
		display: block;
		margin: auto;
	}

	nav ul li {
		float: left;
	}

	nav ul li a {
		font-family: 'Noto Sans', sans-serif;
		display: block;
		color: black;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
	}

	nav ul li a:hover {
	}

	nav ul li a.side {
		padding: 14px 16px;
		border-left:1px solid silver;
		height: 20px;
		font-weight: bold;
		font-size: 14px;
		color: #757272;;
	}

	nav ul li a.side.last {
		border-right:1px solid silver;
	}

	nav ul li a.side:hover {
		background-color: white;
		height: 23px;
	}

	/* LOGO */
	nav ul li .logo:hover {
		height: 22px;
	}

	nav ul li .logo{
		color:#35b335;
		font-size: 25px;
		font-weight: bold;
		font-family: 'Noto Sans', sans-serif;
		width: 87px;
	}


	/* INPUT */
	nav ul li{
	}
	nav ul li.side{
		float: right;
		margin-left: 0px;
	}
	nav ul li.search{
		width: 332px;
	}
	input.search{
		height: 20px;
		padding: 7px;
		margin: 6px;
		border: 1px solid silver;
		border-top-left-radius: 5px;
		border-bottom-left-radius:5px;
		width: 100%;;
	}
	select.search{
		font-size: 11px;
		color: #6b5f5f;
		height: 36px;
		padding: 7px;
		margin: 6px;
		border: 1px solid silver;
		width: 101%;;
	}
	button.search{
		height: 36px;
		padding: 7px;
		margin: 6px;
		border-top-right-radius: 5px;
		border-bottom-right-radius:5px;
		width: 34px;
		background-color: #f4f4f4;
	}
	button.search img{
		width: 20px;
		height: 20px;
		zoom:0.9;
	}


</style>
<nav>
	<ul class="container">
		<li><a href="index.php" class="logo">marketarea</a></li>
		<li><a href="index.php" class="logo"><img src="lastdomain.png" style="height: 30px;"></a></li>
		<li class="search"><input type="text" name="" placeholder="Search product or shop" class="search"></li>
		<li><select class="search"><option>All Category</option></select></li>
		<li><button class="search"><img src="search-icon.png"></button></li>
		<li class="side"><a href="index.php" class="side last">Sign In</a></li>
		<li class="side"><a href="index.php" class="side">Sign Up</a></li>
		<li class="side"><a href="index.php" class="side">Help</a></li>
	</ul>
</nav>
