<?php
	/**
	* MODUL SLIDE SHOW BY IBID
	* 
	* MODAY, 3 OCT 2016
	* 
	*/
	class slideshow
	{
		private $duration=2500,$link,$path;

		function __construct($link,$path) {
			$this->link=$link;
			$this->path=$path;
		}

		public function show()
		{
			# code...
			$duration = $this->duration;
			$link = $this->link;
			$path = $this->path;
			?>
			<link rel="stylesheet" href="swiper.min.css">
			<style type="text/css">
				/* SWIPE */
				.swiper-container {
					margin-top:10px;
					width: 960px;
					height: 100%;
					margin-left: auto;
					margin-right: auto;
				}
				.swiper-slide {
					text-align: center;
					font-size: 18px;
					background: #fff;
					/* Center slide text vertically */
					display: -webkit-box;
					display: -ms-flexbox;
					display: -webkit-flex;
					display: flex;
					-webkit-box-pack: center;
					-ms-flex-pack: center;
					-webkit-justify-content: center;
					justify-content: center;
					-webkit-box-align: center;
					-ms-flex-align: center;
					-webkit-align-items: center;
					align-items: center;
				}
			</style>
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<?php
					for($i=0;$i<count($path);$i++)
					{

						echo '<div class="swiper-slide"><a href="'.$link[$i].'"><img src="'.$path[$i].'"></a></div>';
					}
					?>

				</div>
				<!-- Add Pagination -->
				<div class="swiper-pagination"></div>
				<!-- Add Arrows -->
				<div class="swiper-button-next"></div>
				<div class="swiper-button-prev"></div>
			</div>

			<!-- Swiper JS -->
			<script src="swiper.min.js"></script>
			<!-- Initialize Swiper -->
			<script>
				var swiper = new Swiper('.swiper-container', {
					pagination: '.swiper-pagination',
					nextButton: '.swiper-button-next',
					prevButton: '.swiper-button-prev',
					paginationClickable: true,
					spaceBetween: 30,
					centeredSlides: true,
					autoplay: <?php echo $duration ?>,
					autoplayDisableOnInteraction: false
				});
			</script>
			<?php
		}

		public function setDuration($duration){
			$this->duration = $duration;
		}
	}

	?>