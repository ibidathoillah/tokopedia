
              
            <?php
            $lib -> set_title("Belanja Online Aman dan Bebas Penipuan");
             $lib -> set_description("Belanja Online di Tokopedia itu lebih aman dan bebas penipuan, karena pembayaran Anda baru diteruskan kepada pihak penjual setelah barang Anda terima. Anda juga dapat menemukan harga-harga termurah dari jutaan ragam produk yang didaftarkan oleh toko-toko online paling terpercaya di Indonesia.");
             $lib -> get_head();
             $lib -> add("");
              ?>
               <link rel="stylesheet" href="http://<?php echo $domain ?>/lib/css/style_tokopedia.css" type="text/css"/>
              
            <body itemscope itemtype="http://schema.org/WebPage">
               <header id="header" class="row-fluid">
                  <div class="container mt-10 mb-10">
                     <div class="topbar-logo-wrapper pull-left span3"><a class="site-logo" href="https://www.tokopedia.com">Tokopedia</a></div>
                     <div class="btn-share pull-right span6">
                        <ul class="inline social-buttons ceef">
                           <li class="fb-share"><a class="socialite facebook-like fb-like" data-href="https://www.tokopedia.com/bebas-penipuan" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"><span class="vhidden">Share on Facebook</span></a></li>
                           <li><a href="http://twitter.com/share" class="socialite twitter-share" data-url="https://www.tokopedia.com/bebas-penipuan" target="_blank" data-hashtags="Bebas Penipuan, Tokopedia" data-text="Belanja Online Aman dan Bebas Penipuan | Tokopedia"><span class="vhidden">Share on Twitter</span></a></li>
                           <li><a href="https://plus.google.com/share?url=https://www.tokopedia.com/bebas-penipuan" class="socialite googleplus-one" data-size="medium" target="_blank"><span class="vhidden">Share on Google+</span></a></li>
                        </ul>
                     </div>
                  </div>
               </header>
               <div class="page_heading row-fluid center_content">
                  <div class="container">
                     <h1>Mengapa Belanja Online di Tokopedia &nbsp;<br>Aman dan Nyaman?</h1>
                     <div class="video_wraper">
                        <div class="video-holder"><iframe width="640" height="360" src="//www.youtube.com/embed/dUh9q_D0NIE" frameborder="0" allowfullscreen></iframe></div>
                     </div>
                  </div>
                  <a class="arrow-beli" href="#aman"></a>
               </div>
               <div class="container-fluid" id="aman">
                  <div class="container">
                     <div class="row-fluid">
                        <div class="span12">
                           <div class="span6"><img src="https://ecs7.tokopedia.net/img/micrositebeli/scroll/aman-new.png" alt="Belanja online lebih aman"></div>
                           <div class="span6 txt-beli">
                              <h3>Lebih Aman</h3>
                              <p>Belanja online di Tokopedia itu lebih aman dan bebas penipuan, karena pembayaran Anda baru diteruskan kepada pihak penjual setelah barang Anda terima. Lewat fasilitas rekening bersama gratis ini, Anda pun bebas dari para penipu-penipu online dengan identitas tidak jelas.</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="container-fluid container-fluid--right">
                  <div class="container">
                     <div class="row-fluid">
                        <div class="span12">
                           <div class="span6 image"><img src="https://ecs7.tokopedia.net/img/micrositebeli/scroll/banyak-pilihan-barang-new.png" alt="Beragam Produk ribuan penjual"></div>
                           <div class="span6 txt-beli">
                              <h3>Banyak Pilihan</h3>
                              <p>Sebagai mal online terbesar Indonesia, tempat berkumpulnya toko-toko online terpercaya di Indonesia. Di Tokopedia ada jutaan ragam produk yang siap kamu beli! Tanpa perlu macet dan bisa kamu tracking lho barangnya, sudah sampai di mana secara online!</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="container-fluid">
                  <div class="container">
                     <div class="row-fluid">
                        <div class="span12">
                           <div class="span6"><img src="https://ecs7.tokopedia.net/img/micrositebeli/scroll/harga-paling-murah-new.png" alt="Harganya paling murah"></div>
                           <div class="span6 txt-beli">
                              <h3>Harganya paling murah</h3>
                              <p>Belanja online di Tokopedia harganya sudah pasti paling murah! Dan murah tidak berarti murahan, karena kamu bisa melihat secara transparan ulasan atau review dari para pembeli sebelumnya. Tersedia juga harga grosir! Buktikan sendiri murahnya Tokopedia!</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <footer>
                  <div class="container">
                     <div class="pull-left mt-10 ml-5">2016  &copy; <a title="Tokopedia" target="_blank" href="https://www.tokopedia.com">PT Tokopedia</a></div>
                     <div class="pull-right mt-5"><a class="pull-left mr-5" title="Facebook" href="https://www.facebook.com/tokopedia" target="_blank"><span><img src="https://ecs7.tokopedia.net/img/micrositebeli/scroll/facebook-icon.png" alt=""></span></a><a class="pull-left mr-5" title="Twitter" href="http://twitter.com/tokopedia" target="_blank"><span><img src="https://ecs7.tokopedia.net/img/micrositebeli/scroll/twitter-icon.png" alt=""></span></a><a class="pull-left mr-5" title="Google Plus" href="http://gplus.to/tokopedia" target="_blank"><span><img src="https://ecs7.tokopedia.net/img/micrositebeli/scroll/google-plus-icon.png" alt=""></span></a><a class="pull-left mr-5" title="Instagram" href="https://instagram.com/tokopedia" target="_blank"><span><img src="https://ecs7.tokopedia.net/img/microsite/instagram.png" alt="Instagram"></span></a><a class="pull-left mr-5" title="Tokopedia" href="https://www.tokopedia.com" target="_blank"><span><img src="https://ecs7.tokopedia.net/img/micrositebeli/scroll/tokopedia-icon.png" alt=""></span></a></div>
                  </div>
               </footer>
               <div class="cta row-fluid">
                  <div class="container">
                     <img src="https://ecs7.tokopedia.net/img/newtkpd/buble-owl-color-tokopedia.png" class="pull-left hidden-phone">
                     <div class="span8">
                        <h3>Buktikan murah dan lengkap nya barang-barang di Tokopedia!</h3>
                        <form action="https://www.tokopedia.com/s-c.pl" method="post" class="span12 m-0"><input type="text" value="" name="search_keyword" id="emailInput" class="span5" placeholder="Masukan nama produk"><input type="submit" value="Temukan Produk" name="subscribe" id="emailbtn" class="button_big"></form>
                     </div>
                  </div>
               </div>
            </body>
         </html>