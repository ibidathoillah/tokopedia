<?php
if(isset($access)){

  if($access=="public")
  {
    $lib -> get_head("Login","For Login");
?>
<link href="http://localhost/tokopedia/lib/css/login.css" rel="stylesheet">

<body>
    <form target="_top" id="header-frm-login" class="form-login p-30 box-white" method="post" action="">
        <div class="row-fluid control-group mt-10">
            <input type="hidden" name="theme" value="iframe">
            <input autofocus="autofocus" type="text" id="inputEmail" name="email" placeholder="Email" value="" class=" input-login w-100p mb-5">
        </div>
        <div class="row-fluid control-group">
            <input type="password" id="inputPassword" name="password" placeholder="Kata Sandi" class="input-login w-100p mb-0" autocomplete="off">
        </div>
        
        <div class="row-fluid control-group">
            <div class="span7">
                <div>
                    <label class="span12">
                        <input class="icheck" type="checkbox" name="remember_me">
                        <small class="small-gray">&nbsp;&nbsp;&nbsp;Biarkan saya tetap masuk</small>
                    </label>
                </div>
            </div>
            <div class="span5">
                <div class="pull-right">
                    <small><a href="https://accounts.tokopedia.com/reset" id="reset-password">
                      Lupa Kata Sandi?</a>
                  </small>
              </div>
          </div>

          

          <button id="global_login_btn" type="submit" class="btn btn-block btn-action btn-login-top mt-10">Masuk ke Tokopedia</button>
      </div>
  </form>

    <?php
  }
}
else
{
  echo "No Access";
}

?>