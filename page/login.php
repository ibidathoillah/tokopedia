<?php
if(isset($access)){

  if($access=="public")
  {
    $lib -> set_title("Login");
    $lib -> set_description("Please log in / sign in to Tokopedia. Enjoy the shopping experience enjoyable and safe for Tokopedia fitted together account for free!");
    $lib -> get_head();
?>
<div id="content-container" class="pt_auto">
      <div id="verify_number"></div>
      
      <div id="sticky-div" class="none">
        <div class="row-fluid mb-10 alert-info container ">
          <div id="sticky-note-container">
            <div class="pull-left mr-15 white label-new-info"><strong><span class="mr-5 icon-container-circle"><i class="icon-notification fs-11"></i></span><i>INFO TERBARU</i></strong></div>
            <ul class="newsticker" id="sticky-note"></ul>
          </div>
        </div>
      </div>
      <script type="text/javascript">var active_page  = 'product';</script>
      <div class="container" itemscope="" itemtype="http://schema.org/WebPage">
        <div class="row-fluid"></div>
      </div>
      <section class="container mb-20">
        <div class="row-fluid mt-0">
          <div class="text-center mb-10">
            <div class="logo">
              <a href="https://www.tokopedia.com"><span class="site-logo"></span></a>
            </div>
          </div>
          <h2 class="text-center">Masuk Tokopedia</h2>
          <p class="text-center fs-14">Belum punya akun Tokopedia? Daftar
            <a href="http://<?php echo $domain ?>/register" class="green underline">di sini</a>
          </p>
        </div>
        
        <div class="row-fluid mt-20 pt-20">
          <div class="span5">
            <div class="login-alt">
              <a href="fb">
                <button class="btn btn-block btn-facebook mb-10" type="button">
                  <i class="icon-facebook icon-large"></i>&nbsp;&nbsp;&nbsp;Masuk Dengan Facebook
                </button>
              </a>
              <a href="google">
                <button class="btn btn-block btn-buy mb-10" type="button">
                  <i class="icon-google-plus icon-large"></i>&nbsp;&nbsp;&nbsp;Masuk Dengan Google
                </button>
              </a>
              <a href="yahoo">
                <button class="btn btn-block btn-yahoo" type="button">
                  <i class="icon-yahoo icon-large"></i>&nbsp;&nbsp;&nbsp;Masuk Dengan Yahoo
                </button>
              </a>
            </div>
          </div>
          <div class="span2 ">
            <div class="line1">
            </div>
            <p class="text-center mt-20 mb-20 small-gray">atau</p>
            <div class="line2"></div>
          </div>
          <div class="span5">
            <form method="post" action="https://accounts.tokopedia.com/authorize?response_type=code&amp;client_id=1001&amp;state=eyJyZWYiOiJodHRwczovL3d3dy50b2tvcGVkaWEuY29tLyIsInV1aWQiOiJiY2E4ZWE0MC1iYzljLTQ1OTAtYWVjMS1kYWMyYmM1OGE1ZjQifQ&amp;redirect_uri=https%3a%2f%2faccounts.tokopedia.com%2fappauth%2fcode&amp;theme=default&amp;scope=">
              <style>#register ul li {position: relative;margin-bottom: 10px;}</style>
              <fieldset id="register">
                <ul class="mt-70 pt-30">
                  <li>
                  
                  </li>
                  <li><i class="icon-message-alt small-gray"></i>
                    <input type="text" id="inputEmail" name="email" placeholder="Email" value="" autofocus="autofocus">
                  </li>
                  <li><i class="icon-locked small-gray"></i>
                    <input type="password" id="inputPassword" name="password" placeholder="Kata Sandi" autocomplete="off">
                  </li>
                  <li>
                    <label class="span6">
                      <div class="icheckbox_square-green" style="position: relative;"><input class="icheck" name="remember_me" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                      <ins class="iCheck-helper"></ins>
                      <label>&nbsp;Biarkan saya tetap masuk</label>
                    </label>
                    <div class="span4 pr-10">
                      <a href="/reset" class="green underline fs-12 pull-right mr-10">Lupa Kata Sandi</a>
                    </div>
                  </li>
                  <li>
                    
                  </li>
                  <li>
                    <button type="submit" class="btn btn-action mb-20">Masuk ke Tokopedia</button>
                  </li>
                </ul>
              </fieldset>
            </form>
          </div>
        </div>
      </section>
    </div>

    <?php
  }
}
else
{
  echo "No Access";
}

?>