<?php
if(isset($access)){

   if($access=="admin"){

          $lib -> set_title("Administrator");
          $lib -> set_description("Please log in / sign in to Administrator. No entry except administrator");
          $lib -> get_head();
      ?>
      <link rel="stylesheet" href="http://<?php echo $domain ?>/lib/css/swiper.min.css">
      <div style="zoom:1.3;font-size: 20px;position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);">
         <div >

         <center><div style="background: url(http://<?php echo $domain ?>/lib/img/dv3-sprite2.png) no-repeat transparent;    background-position: -1px -1px;
           width: 145px;
           height: 31px;
           display: block;"></div>


           <form method="post" action="">
            <style>#register ul li {position: relative;margin-bottom: 10px;}</style>
            <fieldset id="register">
               <ul class="mt-70 pt-0">
                  <li><i class="icon-message-alt small-gray"></i>
                     <input type="text" id="inputEmail" name="email" placeholder="Email" value="" autofocus="autofocus">
                  </li>
                  <li><i class="icon-locked small-gray"></i>
                     <input type="password" id="inputPassword" name="password" placeholder="Kata Sandi" autocomplete="off">
                  </li>
                  <li>
                     <div class="span4 pr-10">
                        <br>
                        <button type="submit" class="btn btn-action"">Sign Administrator</button>

                     </div>

                  </li>
               </ul>
            </fieldset>
         </form>
      </div>
   </div>
   <?php

}
elseif($access=="public")
{
   $lib -> get_head("","");

   ?>
   <body class="home-only">
      <header class="navbar navbar-fixed-top">
         <div class="navbar-inner">
            <div class="container clearfix">
               <div class="topbar-logo-wrapper"><a href="http://<?php echo $domain ?>/" target="_blank">Tokopedia<span class="site-logo"></span></a></div>
               <div class="pull-right">
                  <ul id="b-nav" class="topbar-nav">
                     <li class="dropdown border-left"><a href="http://<?php echo $domain ?>/bantuan/">Help</a></li>
                     <li class="dropdown-single border-left"><a href="http://<?php echo $domain ?>/register">Register</a></li>
                     <li class="dropdown dropdown-right border-both" id="iframe-login">
                        <a href="http://<?php echo $domain ?>/login" id="login-ddl-link" class="dropdown-toggle" onclick="javascript:return false;" data-toggle="dropdown">Login</a>
                        <ul class="dropdown-menu dropdown-basic" role="menu" aria-labelledby="dropdownMenu" id="accounts-menu">
                           <li><input type="hidden" id="accounts-url" name="accounts_url" value="http://<?php echo $domain ?>/mini_login"><iframe style="height: 300px" id="iframe-accounts" src=""></iframe></li>
                        </ul>
                     </li>
                  </ul>
               </div>
               <input type="hidden" name="unique_id" value="">
               <form id="navbar-search" action="http://<?php echo $domain ?>/search" class="navbar-search" method="get">
                  <input type="hidden" name="st" value="product">
                  <div class="search-parent-older">
                     <div class="searchform search-parent">
                        <input name="q" class="input-wrapper" id="search-keyword" type="text" placeholder="Search Products or Stores" autocomplete="off" value="">
                        <div id="div_autocomplete" style="position:relative"></div>
                        <div class="cat-wrapper permanent-active">
                           <select name="sc" id="cat-select" style="margin-bottom: 0;color: #777;
                           font-size: 11px;
                           white-space: nowrap;
                           text-indent: 1px;width: 120px;padding-bottom: 12px;">
                           <option value="0">All Category</option>
                           <option class="ml-10" value="79">Fashion &amp; Accessoris</option>
                           <option class="ml-10" value="78">Pakaian</option>
                           <option class="ml-10" value="61">Kecantikan</option>
                           <option class="ml-10" value="715">Kesehatan</option>
                           <option class="ml-10" value="984">Rumah Tangga</option>
                           <option class="ml-10" value="983">Dapur</option>
                           <option class="ml-10" value="56">Perawatan Bayi</option>
                           <option class="ml-10" value="65">Handphone &amp; Tablet</option>
                           <option class="ml-10" value="288">Laptop &amp; Aksesoris</option>
                           <option class="ml-10" value="297">Komputer &amp; Aksesoris</option>
                           <option class="ml-10" value="60">Elektronik</option>
                           <option class="ml-10" value="578">Kamera, Foto &amp; Video</option>
                           <option class="ml-10" value="63">Otomotif</option>
                           <option class="ml-10" value="62">Olahraga</option>
                           <option class="ml-10" value="642">Office &amp; Stationery</option>
                           <option class="ml-10" value="54">Souvenir, Kado &amp; Hadiah</option>
                           <option class="ml-10" value="55">Mainan &amp; Hobi</option>
                           <option class="ml-10" value="35">Makanan &amp; Minuman</option>
                           <option class="ml-10" value="8">Buku</option>
                           <option class="ml-10" value="20">Software</option>
                           <option class="ml-10" value="57">Film, Musik &amp; Game</option>
                           <option class="ml-10" value="36">Produk Lainnya</option>
                        </select>
                     </div>
                     <span class="btn-search-wrapper"><button class="btn btn-search" type="submit"><i class="icon-search icon-large orange"></i></button></span>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </header>
   <!--SLIDE SHOW -->
   <?php
   $table = $config -> set_tabel("slideshow");
   $sql -> query("SELECT * FROM $table");
   ?>
   <div class="home-slider">
      <div class="home-slider_carousel owl-carousel owl-theme owl-loaded">
         <div  class="owl-stage-outer">
            <div  class="owl-stage">
               <div class="swiper-wrapper">
                  <?php
                  while ($data = $sql -> fetch_array()) {
                     ?>
                     <div class="swiper-slide"><a href="<?php echo $data['link']; ?>"><img src="http://<?php echo $domain; ?>/lib/image/<?php echo $data['img']; ?>"></a></div>
                     <?php
                  }
                  ?>
               </div>
               <!-- Add Arrows -->
               <div class="home-slider_nav"><a class="prev"><i class="icon-chevron-left-alt"></i></a><a class="next"><i class="icon-chevron-right-alt"></i></a></div>
               <div style="margin-bottom: 50px;"></div>
               <!-- Add Pagination -->
               <div class="swiper-pagination"></div>
            </div>
         </div>
      </div>
   </div>
   <!-- Swiper JS -->
   <script src="http://<?php echo $domain ?>/lib/js/swiper.min.js"></script>
   <!-- Initialize Swiper -->
   <script>
      var swiper = new Swiper('.owl-stage', {
        pagination: '.swiper-pagination',
        nextButton: '.next',
        prevButton: '.prev',
        paginationClickable: true,
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: 1500,
        autoplayDisableOnInteraction: false
     });
  </script>
  <div  id="content-container"  >
   <section class="all_cat_container">
      <div class="container">
         <div class="row-fluid">
            <div class="span12">
               <p class="pull-left font-bold fs-13 mb-0">Select Category</p>
               <a class="pull-right fs-11" href="http://<?php echo $domain ?>/p">All Categories &raquo;</a>
            </div>
         </div>
         <div class="row-fluid">
            <div class="span12">
               <ul class="allcat">
                  <li><a href="http://<?php echo $domain ?>/p/fashion-aksesoris"><i class="sprite-product-fashion sprite-cat"></i><span class="title">Fashion &amp; Aksesoris </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/pakaian"><i class="sprite-product-clothing sprite-cat"></i><span class="title">Pakaian </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/kecantikan"><i class="sprite-product-beauty sprite-cat"></i><span class="title">Kecantikan </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/kesehatan"><i class="sprite-product-health sprite-cat"></i><span class="title">Kesehatan </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/rumah-tangga"><i class="sprite-product-home-appliances sprite-cat"></i><span class="title">Rumah Tangga </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/dapur"><i class=" sprite-product-kitchen-dining sprite-cat"></i><span class="title">Dapur </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/perawatan-bayi"><i class="sprite-product-baby sprite-cat"></i><span class="title">Perawatan Bayi </span></a></li>
               </ul>
               <ul class="allcat">
                  <li><a href="http://<?php echo $domain ?>/p/handphone-tablet"><i class="sprite-product-phone-tablet sprite-cat"></i><span class="title">Handphone &amp; Tablet </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/laptop-aksesoris"><i class="sprite-product-notebook sprite-cat"></i><span class="title">Laptop &amp; Aksesoris </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/komputer-aksesoris"><i class="sprite-product-pc sprite-cat"></i><span class="title">Komputer &amp; Aksesoris </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/elektronik"><i class="sprite-product-electronics sprite-cat"></i><span class="title">Elektronik </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/kamera-foto-video"><i class="sprite-product-photography sprite-cat"></i><span class="title">Kamera, Foto &amp; Video </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/otomotif"><i class="sprite-product-automotive sprite-cat"></i><span class="title">Otomotif </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/olahraga"><i class="sprite-product-sports sprite-cat"></i><span class="title">Olahraga </span></a></li>
               </ul>
               <ul class="allcat">
                  <li><a href="http://<?php echo $domain ?>/p/office-stationery"><i class=" sprite-product-office-stationery sprite-cat"></i><span class="title">Office &amp; Stationery </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/souvenir-kado-hadiah"><i class=" sprite-product-gift sprite-cat"></i><span class="title">Souvenir, Kado &amp; Hadiah </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/mainan-hobi"><i class="sprite-product-toys-hobbies sprite-cat"></i><span class="title">Mainan &amp; Hobi </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/makanan-minuman"><i class="sprite-product-food-beverages sprite-cat"></i><span class="title">Makanan &amp; Minuman </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/buku"><i class="sprite-product-books sprite-cat"></i><span class="title">Buku </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/software"><i class="sprite-product-softwares sprite-cat"></i><span class="title">Software </span></a></li>
                  <li><a href="http://<?php echo $domain ?>/p/film-musik-game"><i class="sprite-product-movies-games-music sprite-cat"></i><span class="title">Film, Musik &amp; Game </span></a></li>
               </ul>
               <ul class="allcat">
                  <li><a href="https://pulsa.tokopedia.com/?utm_source=desktop&utm_medium=categories%20before%20log%20in&utm_campaign=pulsa%20icon"><i class="sprite-product-recharge sprite-cat"></i><span class="title">Pulsa</span><span class="cat-new-label">NEW</span></a></li>
               </ul>
               <ul class="allcat">
                  <li><a href="https://pulsa.tokopedia.com/token-listrik/?utm_source=desktop&utm_medium=categories%20before%20log%20in&utm_campaign=token%20listrik%20icon"><i class="sprite-product-token-listrik sprite-cat"></i><span class="title">Token Listrik</span><span class="cat-new-label">NEW</span></a></li>
               </ul>
               <ul class="allcat">
                  <li><a href="https://tiket.tokopedia.com/kereta-api/?utm_source=desktop&utm_medium=categories%20before%20log%20in&utm_campaign=tiket%20kereta%20icon"><i class="sprite-product-train sprite-cat"></i><span class="title">Tiket Kereta Api</span><span class="cat-new-label">NEW</span></a></li>
               </ul>
            </div>
         </div>
      </div>
   </section>
   <section class="container">
      <div class="row-fluid mb-5">
         <div class="span12">
            <div class="line-through">
               <p><span class="font-bold fs-16">Hot List</span></p>
            </div>
         </div>
      </div>
      <div class="row-fluid mb-20">
         <div class="span4">
            <a aria-hidden="true" tabindex="-1" href="http://<?php echo $domain ?>/hot/sketchbook" class="clearfix display-block">
               <picture>
                  <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/5/17/13/29/318724/sketchbook.jpg.webp">
                     <img alt="Sketchbook" src="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/5/17/13/29/318724/sketchbook.jpg">
                  </picture>
                  <div class="clearfix mt-5">
                     <div class="hotlist-left" itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">
                        <div class="mt-5 fs-12 ellipsis">Sketchbook</div>
                        <div><small class="muted mr-5">Start From</small><span class="red fs-14" itemprop="lowPrice">Rp 25rb</span></div>
                     </div>
                     <div class="hotlist-thumbnail">
                        <ul class="inline pull-right">
                           <li>
                              <picture>
                                 <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/10/2/3394/3394_150aa9ab-4040-40d1-ae4a-548507ac4dc3.jpg.webp">
                                    <img alt="Nature Collection Sketch Book Small / Buku Catatan" src="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/10/2/3394/3394_150aa9ab-4040-40d1-ae4a-548507ac4dc3.jpg" class="img-40 img-frame">
                                 </picture>
                              </li>
                              <li>
                                 <picture>
                                    <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/9/16/10812173/10812173_dc4ec544-d8d3-479c-ae8d-d555e3979112.jpg.webp">
                                       <img alt="Feathers Watercolor Sketchbook/ Sketchbook / Buku Gambar Limited" src="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/9/16/10812173/10812173_dc4ec544-d8d3-479c-ae8d-d555e3979112.jpg" class="img-40 img-frame">
                                    </picture>
                                 </li>
                                 <li>
                                    <picture>
                                       <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/10/2/3394/3394_0870baa1-f547-4bac-8ae5-5180a88ffca6.jpg.webp">
                                          <img alt="Papeterie Sketch Book / Buku Gambar / Sketsa / Catatan" src="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/10/2/3394/3394_0870baa1-f547-4bac-8ae5-5180a88ffca6.jpg" class="img-40 img-frame">
                                       </picture>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </a>
                     </div>
                     <div class="span4">
                        <a aria-hidden="true" tabindex="-1" href="http://<?php echo $domain ?>/hot/copic-marker" class="clearfix display-block">
                           <picture>
                              <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/5/16/35/22/318724/copic-marker.jpg.webp">
                                 <img alt="Copic Marker" src="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/5/16/35/22/318724/copic-marker.jpg">
                              </picture>
                              <div class="clearfix mt-5">
                                 <div class="hotlist-left" itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">
                                    <div class="mt-5 fs-12 ellipsis">Copic Marker</div>
                                    <div><small class="muted mr-5">Start From</small><span class="red fs-14" itemprop="lowPrice">Rp 50rb</span></div>
                                 </div>
                                 <div class="hotlist-thumbnail">
                                    <ul class="inline pull-right">
                                       <li>
                                          <picture>
                                             <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/10/3/12380058/12380058_b25b86f9-1ba2-4635-812f-58ce1f5e2238.jpg.webp">
                                                <img alt="COPIC Marker 36 pcs Basic Colors Set Termurah terlaris" src="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/10/3/12380058/12380058_b25b86f9-1ba2-4635-812f-58ce1f5e2238.jpg" class="img-40 img-frame">
                                             </picture>
                                          </li>
                                          <li>
                                             <picture>
                                                <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/3/29/3829033/3829033_a9e0140e-4479-484d-8c7d-6705d1fd7c75.jpg.webp">
                                                   <img alt="COPIC Sketch colorless blender - Ref.CSM/0" src="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/3/29/3829033/3829033_a9e0140e-4479-484d-8c7d-6705d1fd7c75.jpg" class="img-40 img-frame">
                                                </picture>
                                             </li>
                                             <li>
                                                <picture>
                                                   <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/9/27/436074/436074_637b6da7-3d84-4428-800c-dacbad392d17.webp">
                                                      <img alt="COPIC Dual-Ended Marker : Cerah Ceria" src="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/9/27/436074/436074_637b6da7-3d84-4428-800c-dacbad392d17" class="img-40 img-frame">
                                                   </picture>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                                 <div class="span4">
                                    <a aria-hidden="true" tabindex="-1" href="http://<?php echo $domain ?>/hot/kuretake-pen" class="clearfix display-block">
                                       <picture>
                                          <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/5/16/29/44/318724/kuretake-pen.jpg.webp">
                                             <img alt="Kuretake Pen" src="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/5/16/29/44/318724/kuretake-pen.jpg">
                                          </picture>
                                          <div class="clearfix mt-5">
                                             <div class="hotlist-left" itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">
                                                <div class="mt-5 fs-12 ellipsis">Kuretake Pen</div>
                                                <div><small class="muted mr-5">Start From</small><span class="red fs-14" itemprop="lowPrice">Rp 20rb</span></div>
                                             </div>
                                             <div class="hotlist-thumbnail">
                                                <ul class="inline pull-right">
                                                   <li>
                                                      <picture>
                                                         <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/10/5/7880770/7880770_2db6b24e-7b0b-4f54-b609-45c393ea38f3.jpg.webp">
                                                            <img alt="Kuretake Zig Cartoonist Mangaka Outline Pen - 005 Black Berkualitas" src="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/10/5/7880770/7880770_2db6b24e-7b0b-4f54-b609-45c393ea38f3.jpg" class="img-40 img-frame">
                                                         </picture>
                                                      </li>
                                                      <li>
                                                         <picture>
                                                            <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/10/5/7396202/7396202_b4d89402-67c3-482f-8d86-622d38221379.jpg.webp">
                                                               <img alt="Kuretake Bimoji Brush Pen - Large Murah" src="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/10/5/7396202/7396202_b4d89402-67c3-482f-8d86-622d38221379.jpg" class="img-40 img-frame">
                                                            </picture>
                                                         </li>
                                                         <li>
                                                            <picture>
                                                               <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/10/5/2852912/2852912_70c27b21-a813-40af-a71b-2826a686bb29.jpg.webp">
                                                                  <img alt="Kuretake Zig Fudebiyori Metallic Brush Pen Limited" src="https://ecs7.tokopedia.net/img/cache/100-square/product-1/2016/10/5/2852912/2852912_70c27b21-a813-40af-a71b-2826a686bb29.jpg" class="img-40 img-frame">
                                                               </picture>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </a>
                                             </div>
                                          </div>
                                          <div class="row-fluid mb-20">
                                             <div class="span4">
                                                <a aria-hidden="true" tabindex="-1" href="http://<?php echo $domain ?>/hot/xiaomi-piston" class="clearfix display-block">
                                                   <picture>
                                                      <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/5/16/18/57/318724/xiaomi-piston.jpg.webp">
                                                         <img alt="Xiaomi Piston" src="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/5/16/18/57/318724/xiaomi-piston.jpg">
                                                      </picture>
                                                      <div class="clearfix mt-5">
                                                         <div class="hotlist-left" itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">
                                                            <div class="mt-5 fs-12 ellipsis">Xiaomi Piston</div>
                                                            <div><small class="muted mr-5">Start From</small><span class="red fs-14" itemprop="lowPrice">Rp 95rb</span></div>
                                                         </div>
                                                         <div class="hotlist-thumbnail">
                                                            <ul class="inline pull-right"></ul>
                                                         </div>
                                                      </div>
                                                   </a>
                                                </div>
                                                <div class="span4">
                                                   <a aria-hidden="true" tabindex="-1" href="http://<?php echo $domain ?>/hot/lumee-case" class="clearfix display-block">
                                                      <picture>
                                                         <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/5/16/11/37/4659/lumee-case.jpg.webp">
                                                            <img alt="Lumee Case" src="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/5/16/11/37/4659/lumee-case.jpg">
                                                         </picture>
                                                         <div class="clearfix mt-5">
                                                            <div class="hotlist-left" itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">
                                                               <div class="mt-5 fs-12 ellipsis">Lumee Case</div>
                                                               <div><small class="muted mr-5">Start From</small><span class="red fs-14" itemprop="lowPrice">Rp 150rb</span></div>
                                                            </div>
                                                            <div class="hotlist-thumbnail">
                                                               <ul class="inline pull-right"></ul>
                                                            </div>
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <div class="span4">
                                                      <a aria-hidden="true" tabindex="-1" href="http://<?php echo $domain ?>/hot/iring" class="clearfix display-block">
                                                         <picture>
                                                            <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/5/16/15/15/4430/iring.jpg.webp">
                                                               <img alt="iRing" src="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/5/16/15/15/4430/iring.jpg">
                                                            </picture>
                                                            <div class="clearfix mt-5">
                                                               <div class="hotlist-left" itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">
                                                                  <div class="mt-5 fs-12 ellipsis">iRing</div>
                                                                  <div><small class="muted mr-5">Start From</small><span class="red fs-14" itemprop="lowPrice">Rp 10rb</span></div>
                                                               </div>
                                                               <div class="hotlist-thumbnail">
                                                                  <ul class="inline pull-right"></ul>
                                                               </div>
                                                            </div>
                                                         </a>
                                                      </div>
                                                   </div>
                                                   <div class="row-fluid mb-20">
                                                      <div class="span4">
                                                         <a aria-hidden="true" tabindex="-1" href="http://<?php echo $domain ?>/hot/xiaomi-yin-xiang-bluetooth-speaker" class="clearfix display-block">
                                                            <picture>
                                                               <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/4/17/22/55/318724/xiaomi-yin-xiang-bluetooth-speaker.jpg.webp">
                                                                  <img alt="Xiaomi Yin Xiang Bluetooth Speaker" src="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/4/17/22/55/318724/xiaomi-yin-xiang-bluetooth-speaker.jpg">
                                                               </picture>
                                                               <div class="clearfix mt-5">
                                                                  <div class="hotlist-left" itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">
                                                                     <div class="mt-5 fs-12 ellipsis">Xiaomi Yin Xiang Bluetooth Speaker</div>
                                                                     <div><small class="muted mr-5">Start From</small><span class="red fs-14" itemprop="lowPrice">Rp 380rb</span></div>
                                                                  </div>
                                                                  <div class="hotlist-thumbnail">
                                                                     <ul class="inline pull-right"></ul>
                                                                  </div>
                                                               </div>
                                                            </a>
                                                         </div>
                                                         <div class="span4">
                                                            <a aria-hidden="true" tabindex="-1" href="http://<?php echo $domain ?>/hot/xiaomi-mi-notebook-air" class="clearfix display-block">
                                                               <picture>
                                                                  <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/4/17/19/26/318724/xiaomi-mi-notebook-air.jpg.webp">
                                                                     <img alt="Xiaomi Mi Notebook Air" src="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/4/17/19/26/318724/xiaomi-mi-notebook-air.jpg">
                                                                  </picture>
                                                                  <div class="clearfix mt-5">
                                                                     <div class="hotlist-left" itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">
                                                                        <div class="mt-5 fs-12 ellipsis">Xiaomi Mi Notebook Air</div>
                                                                        <div><small class="muted mr-5">Start From</small><span class="red fs-14" itemprop="lowPrice">Rp 8.7jt</span></div>
                                                                     </div>
                                                                     <div class="hotlist-thumbnail">
                                                                        <ul class="inline pull-right"></ul>
                                                                     </div>
                                                                  </div>
                                                               </a>
                                                            </div>
                                                            <div class="span4">
                                                               <a aria-hidden="true" tabindex="-1" href="http://<?php echo $domain ?>/hot/xiaomi-yi" class="clearfix display-block">
                                                                  <picture>
                                                                     <source type="image/webp" srcset="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/4/17/26/6/3696/xiaomi-yi.jpg.webp">
                                                                        <img alt="Xiaomi Yi" src="https://ecs7.tokopedia.net/img/cache/379/hot_product/2016/10/4/17/26/6/3696/xiaomi-yi.jpg">
                                                                     </picture>
                                                                     <div class="clearfix mt-5">
                                                                        <div class="hotlist-left" itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">
                                                                           <div class="mt-5 fs-12 ellipsis">Xiaomi Yi</div>
                                                                           <div><small class="muted mr-5">Start From</small><span class="red fs-14" itemprop="lowPrice">Rp 1jt</span></div>
                                                                        </div>
                                                                        <div class="hotlist-thumbnail">
                                                                           <ul class="inline pull-right"></ul>
                                                                        </div>
                                                                     </div>
                                                                  </a>
                                                               </div>
                                                            </div>
                                                            <a href="http://<?php echo $domain ?>/hot" class="fs-13 all-hotlist">View All Hot List &raquo;</a>
                                                         </section>
                                                         <section class="container">
                                                            <hr />
                                                            <div class="row-fluid">
                                                               <div class="span12">
                                                                  <div class="span6">
                                                                     <div class="text-left">
                                                                        <p class="mt-15 mb-0 font-bold orange fs-16">Punya Toko Online? Buka cabangnya di Tokopedia</p>
                                                                        <p class="fs-13">Mudah, nyaman dan bebas komisi transaksi. <span class="font-bold">GRATIS!</span></p>
                                                                        <p><a target="_blank" href="http://<?php echo $domain ?>/buka-toko-online-gratis" class="btn btn-action">Buka Toko GRATIS</a><a target="_blank" href="http://<?php echo $domain ?>/jualan-online" class="ml-10 fs-11"><u>Pelajari Lebih Lanjut</u></a></p>
                                                                     </div>
                                                                  </div>
                                                                  <div class="span6"><a target="_blank" href="http://<?php echo $domain ?>/jualan-online"><img class="pull-right" src="https://ecs7.tokopedia.net/img/shop-banner.png" alt="Buka Toko / Online Shop Gratis di Tokopedia"></a></div>
                                                               </div>
                                                            </div>
                                                         </section>
                                                         <div class="container">
                                                            <div class="desc-home-wrapper">
                                                               <h1 class="fs-13 mb-0 lh-20">Tokopedia - Jual Beli Online Aman dan Nyaman</h1>
                                                               <p>Tokopedia merupakan pasar / mal online terbesar di Indonesia yang memungkinkan individu maupun pemilik usaha di Indonesia untuk membuka dan mengelola  <a target="_blank" href="http://<?php echo $domain ?>/jualan-online">toko online</a> mereka secara mudah dan gratis, sekaligus memberikan pengalaman berbelanja online yang lebih aman dan nyaman. Jual beli online menjadi lebih menyenangkan. Punya  <a target="_blank" href="http://<?php echo $domain ?>/jualan-online">online shop</a>? Buka cabang nya di Tokopedia sekarang! Gratis!</p>
                                                            </div>
                                                         </div>
                                                         <section class="container">
                                                            <hr class="mb-15 mt-15">
                                                            <div class="row-fluid">
                                                               <div class="span12">
                                                                  <div class="row-fluid">
                                                                     <div class="span4">
                                                                        <a href="http://<?php echo $domain ?>/free-from-fraud" target="_blank">
                                                                           <img src="https://ecs7.tokopedia.net/img/transparan-01.png" alt="Temukan Produk dari Ribuan Toko / Online Shop terpercaya se Indonesia, dan baca review nya di Tokopedia" class="pull-left mr-10 mt-5" width="110" />
                                                                           <div class="pull-left w-180">
                                                                              <p class="font-bold mt-10 mb-0 fs-15">Transparan</p>
                                                                              <p class="muted">Bandingkan review untuk berbagai online shop terpercaya se-Indonesia</p>
                                                                           </div>
                                                                        </a>
                                                                     </div>
                                                                     <div class="span4">
                                                                        <a href="https://blog.tokopedia.com/2015/08/panduan-keamanan-tokopedia/" target="_blank">
                                                                           <img src="https://ecs7.tokopedia.net/img/aman-02.png" alt="Belanja Online Aman, Bebas Penipuan di Tokopedia" class="pull-left mr-10 mt-5" width="110" />
                                                                           <div class="pull-left w-180">
                                                                              <p class="font-bold mt-10 mb-0 fs-15">Aman</p>
                                                                              <p class="muted">Pembayaran Anda baru diteruskan ke penjual setelah barang Anda terima</p>
                                                                           </div>
                                                                        </a>
                                                                     </div>
                                                                     <div class="span4">
                                                                        <a href="http://<?php echo $domain ?>/free-from-fraud" target="_blank">
                                                                           <img src="https://ecs7.tokopedia.net/img/free-toped.png" alt="Belanja produk apa saja di Tokopedia, gratis tanpa biaya tambahan" class="pull-left mr-10 mt-5" width="110" />
                                                                           <div class="pull-left w-180">
                                                                              <p class="font-bold mt-10 mb-0 fs-15">Fasilitas Escrow Gratis</p>
                                                                              <p class="muted">Fasilitas Escrow (Rekening Bersama) Tokopedia tidak dikenakan biaya tambahan</p>
                                                                           </div>
                                                                        </a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </section>
                                                      </div>
                                                      <footer class="footer-wrapper">
                                                         <div class="footer-main">
                                                            <div class="container">
                                                               <div class="clearfix">
                                                                  <div class="footer-item">
                                                                     <h3 class="fs-14">Tokopedia</h3>
                                                                     <ul>
                                                                        <li><a href="http://<?php echo $domain ?>/about">About Us</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/careers">Careers</a></li>
                                                                        <li><a href="https://blog.tokopedia.com" target="_blank">Blog</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/brand-asset" target="_blank">Media Kit</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/kisah-penjual" target="_blank">Seller Story</a></li>
                                                                        <li><a href="https://tiket.tokopedia.com/kereta-api/" target="_blank">Reserve Train Ticket</a></li>
                                                                        <li><a href="https://events.tokopedia.com/" target="_blank">Events</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/mitra-toppers" target="_blank">Mitra Toppers</a></li>
                                                                     </ul>
                                                                  </div>
                                                                  <div class="footer-item">
                                                                     <h3 class="fs-14">Buy</h3>
                                                                     <ul>
                                                                        <li><a href="http://<?php echo $domain ?>/bebas-penipuan">Why Buy on Tokopedia</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/panduan/beli">How to Buy</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/bantuan/konfirmasi-verifikasi">Payment</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/bantuan/pemesanan-pengiriman-pembeli/pesanan-batal-dana-kemana">Refund</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/hot">Hot List</a></li>
                                                                        <li><a href="https://pulsa.tokopedia.com/" target="_blank">Refill Phone Balance</a></li>
                                                                     </ul>
                                                                  </div>
                                                                  <div class="footer-item">
                                                                     <h3 class="fs-14">Sell</h3>
                                                                     <ul>
                                                                        <li><a href="http://<?php echo $domain ?>/jualan-online">Why Sell on Tokopedia</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/panduan/jual">How to Sell</a></li>
                                                                        <li><a href="https://gold.tokopedia.com/" class="click-gold-footer">Gold Merchant</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/iklan?source=footer&amp;medium=desktop">Advertise</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/bantuan/cara-tarik-dana/tarik-dana">Withdrawal</a></li>
                                                                        <li><a href="https://seller.tokopedia.com/" target="_blank">Seller Center</a></li>
                                                                     </ul>
                                                                  </div>
                                                                  <div class="footer-item">
                                                                     <h3 class="fs-14">Help</h3>
                                                                     <ul>
                                                                        <li><a href="http://<?php echo $domain ?>/terms.pl">Terms and Conditions</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/privacy.pl">Privacy Policies</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/resolution-center">Resolution Center</a></li>
                                                                        <li><a href="http://<?php echo $domain ?>/bantuan">Contact Us</a></li>
                                                                     </ul>
                                                                  </div>
                                                                  <div class="footer-item">
                                                                     <div class="box box-white box-shadow text-center p-10">
                                                                        <a class="app-badges" target="_blank" href="http://<?php echo $domain ?>/mobile-apps">
                                                                           <div class="row-fluid mt-10 ml-5">
                                                                              <div class="span3"><img alt="Tokopedia at Appstore" src="https://ecs7.tokopedia.net/img/footer/mobile-icon.png"></div>
                                                                              <div class="span9">
                                                                                 <h3 class="fs-12 text-left bold mt-10 mb-0 lh-16">Download Tokopedia Mobile Apps&nbsp;&raquo;</h3>
                                                                                 <div class="text-left small-gray">
                                                                                    <div class="pull-left">IOS</div>
                                                                                    <div class="ml-5 mr-5 mt-2 fs-18 pull-left bull">&bull;</div>
                                                                                    <div class="pull-left">Android</div>
                                                                                    <div class="clear-b"></div>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </a>
                                                                        <hr class="mt-15 mb-15">
                                                                        <h3 class="fs-12 mb-10">Follow Us</h3>
                                                                        <div><a target="_blank" href="https://www.facebook.com/tokopedia"><i class="icon-facebook-circle color-facebook fs-20"></i></a>&nbsp; <a target="_blank" href="https://twitter.com/tokopedia"><i class="icon-twitter-circle color-twitter fs-20"></i></a>&nbsp; <a target="_blank" href="https://plus.google.com/+tokopedia/posts"><i class="icon-google-plus-circle color-gplus fs-20"></i></a>&nbsp; <a target="_blank" href="https://www.pinterest.com/tokopedia/"><i class="icon-pinterest-circle color-pinterest fs-20"></i></a>&nbsp; <a target="_blank" href="http://instagram.com/tokopedia"><i class="icon-instagram-circle color-instagram fs-20"></i></a></div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="container footer-additional">
                                                            <div class="row-fluid">
                                                               <div class="span4">
                                                                  <img src="https://ecs7.tokopedia.net/img/footer/toped.png" width="40" class="mr-5" alt="Tokopedia Logo">
                                                                  <div class="inline-block va-middle"><small class="muted">2009 - 2016 &copy; PT Tokopedia </small><br/><small class="muted">Server process time: 0.027 secs.</small></div>
                                                               </div>
                                                               <div class="span4 text-center">
                                                                  <div>
                                                                     <div class="pull-left mt-5 ml-30"><small class="muted">connection &amp; colocation provided by:</small></div>
                                                                     <div class="image-biznet-logo pull-left ml-5"></div>
                                                                  </div>
                                                               </div>
                                                               <div class="span4">
                                                                  <div class="row-fluid">
                                                                     <div class="span5 pull-right">
                                                                        <div class="btn-group pull-right"><a class="btn btn-switch btn-small switch-lang" id="lang-id"><small>Indonesia</small></a> <a class="btn btn-switch btn-small active"><small>English</small></a></div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </footer>
                                                      <?php
                                                   }

                                                }
                                                else
                                                {
                                                   echo "No Access";
                                                }

                                                ?>
