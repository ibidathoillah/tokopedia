<?php
if(isset($access)){

	if($access=="public")
	{
        $lib -> set_title("Register");
        $lib -> set_description("Form Register");
		$lib -> get_head();
		?>

		<div id="content-container" class="pt_auto">
            <div id="verify_number"></div>
            
            <script type="text/javascript">var active_page  = 'product';</script>
            <div class="container" itemscope="" itemtype="http://schema.org/WebPage">
                <div class="row-fluid"></div>
            </div>
            <section class="container mb-20">
                <div class="row-fluid mt-10">
                    <div class="text-center mb-10">
                        <div class="logo">
                            <span class="site-logo"></span>
                        </div>
                    </div>
                    <h2 class="text-center">&nbsp;</h2>
                    <p class="text-center fs-14">Already have an account? Sign in <a href="http://<?php echo $domain ?>/login" class="green underline">here</a></p>
                </div>
                <div class="row-fluid pt-20 mt-20">
                    <div class="span5">
                        <div class="login-alt">
                            <a href="fb"><button class="btn btn-block btn-facebook mb-10" type="button"><i class="icon-facebook icon-large"></i>&nbsp;&nbsp;&nbsp;Register With Facebook</button></a>
                            <a href="google"><button class="btn btn-block btn-buy mb-10" type="button"><i class="icon-google-plus icon-large"></i>&nbsp;&nbsp;&nbsp;Register With Google</button></a>
                            <a href="yahoo"><button class="btn btn-block btn-yahoo" type="button"><i class="icon-yahoo icon-large"></i>&nbsp;&nbsp;&nbsp;Register With Yahoo</button></a>
                        </div>
                    </div>
                    <div class="span2">
                        <div class="line1">
                        </div>
                        <p class="text-center mt-20 mb-20 small-gray">
                            or
                        </p>
                        <div class="line2">
                        </div>
                    </div>
                    <div class="span5">
                        <form id="register-form" name="register_form" method="post" action="">
                            <fieldset id="register">
                                <ul>
                                    <li>
                                        <i class="icon-avatar"></i>
                                        <input type="text" id="full-name" name="full_name" placeholder="Full Name" value="" autofocus="" required>&nbsp;
                                        <span class="icon-spam-alt small-red error-alert error-name none">
                                            <div class="info-tooltip tooltip-red" data-toggle="tooltip" title="" data-original-title="Full Name must be filled."></div>
                                        </span>
                                    </li>
                                    <li>
                                        <i class="icon-smartphone"></i>
                                        <input type="text" id="phone" name="msisdn" placeholder="Mobile Number" value="" class="" required>&nbsp;
                                        <span class="icon-spam-alt small-red error-alert error-phone none">
                                            <div class="info-tooltip tooltip-red" data-toggle="tooltip" title="" data-original-title=""></div>
                                        </span>
                                    </li>
                                    <li>
                                        <i class="icon-message-alt"></i>
                                        <input type="email" id="email" name="email" placeholder="Email Address" value="" class="" required>&nbsp;
                                        <span class="icon-spam-alt small-red error-alert error-email none">
                                            <div class="info-tooltip tooltip-red" data-toggle="tooltip" title="" data-original-title=""></div>
                                        </span>
                                    </li>
                                    <li>
                                        <i class="icon-locked"></i>
                                        <input type="password" id="password" name="password" placeholder="Password" class="" required>&nbsp;
                                        <span class="icon-spam-alt small-red error-alert error-pass none">
                                            <div class="info-tooltip tooltip-red" data-toggle="tooltip" title="" data-original-title=""></div>
                                        </span>
                                    </li>
                                    <li>
                                        <i class="icon-locked"></i>
                                        <input type="password" id="confirm-password" name="confirm_password" placeholder="Confirm Password" class="" required>&nbsp;
                                        <span class="icon-spam-alt small-red error-alert error-pass2 none">
                                            <div class="info-tooltip tooltip-red" data-toggle="tooltip" title="" data-original-title=""></div>
                                        </span>
                                    </li>
                                    <li>
                                        <label class="radio mr-20 ">
                                            <input type="radio" name="gender" id="gender-male" value="1" required>
                                            Male
                                        </label>
                                        <label class="radio ">
                                            <input type="radio" name="gender" id="gender-female" value="2" required>
                                            Female
                                        </label>&nbsp;
                                        <span class="icon-spam-alt small-red error-alert error-gender none">
                                            <div class="info-tooltip tooltip-red" data-toggle="tooltip" title="" data-original-title="" ></div>
                                        </span>
                                    </li>
                                    <li id="bday-date">
                                        <label class="small-gray">Date of Birth</label>&nbsp;
                                        <span class="icon-spam-alt small-red error-alert error-bday none">
                                            <div class="info-tooltip tooltip-red" data-toggle="tooltip" title="" data-original-title=""></div>
                                        </span>
                                        <div>
                                            
                                            <select name="bday_dd" id="sel-date" class="" required>
                                                <option value="">Date</option>
                                                    <?php 
                                                    for($i=1;$i<=31;$i++)
                                                    {
                                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                                    }
                                                    ?>
                                            </select>
                                            <select name="bday_mm" id="sel-month" class="" required>
                                                <option value="">Month</option>
                                                <?php
                                                    for($i=1;$i<=12;$i++)
                                                    {
                                                        $dateObj   = DateTime::createFromFormat('!m', $i);
                                                        $monthName = $dateObj->format('F');
                                                        echo '<option value="'.$i.'">'.$monthName.'</option>';
                                                    }
                                                ?>
                                            </select>
                                            <select name="bday_yy" id="sel-year" class="" required>
                                                <option value="">Year</option>
                                                <?php
                                                for($i=2002;$i>=1936;$i--)
                                                    {
                                                         echo '<option value="'.$i.'">'.$i.'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </li>
                                    <li>

                                        <p class="fs-12 mt-20 mb-20">
                                        <input type="checkbox" name="check" required>
                                            By clicking Register Account, I confirm I have agreed to <a class="green underline" href="https://www.tokopedia.com/terms.pl" target="_blank">Terms &amp; Condition</a>, and <a class="green underline" href="https://www.tokopedia.com/privacy.pl" target="_blank">Security Privacy</a> of Tokopedia.
                                        </p>
                                    </li>
                                    <li>
                                        
                                    </li>
                                    <li>
                                        <button id="register-button" type="submit" name="submit" value="1" class="btn-type-1 btn-action mt-10">Register Account</button>
                                    </li>
                                </ul>
                            </fieldset>
                        </form>

                        <input type="hidden" id="lang" value="en">
                    </div>
                </div>
            </section>
        </div>


		<?php
        require '../lib/php/sendemail.php';

         function is_EmailExist($email)
        {
            global $sql;
            $count = $sql -> get_value ("account","email='".$email."'","count(*)");
            if($count>0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        

        if(isset($_POST['submit'])){

            if(is_EmailExist($_POST['email']))
            {
                ?>
                <script type="text/javascript"> alert("Email telah dipakai")</script>
                <?php
            }
            else
            {
                $sql -> insert("account"," ' ','".$_POST['full_name']."','".$_POST['msisdn']."','".$_POST['email']."','".md5($_POST['password'])."','".$_POST['gender']."',NOW(),NOW(),0,1 ","");
               $id = $sql -> get_value ("account","","MAX(ID)");
               $send = new SendEmail();

               $send -> confirmation($id,$_POST['email'],$_POST['full_name']);
                ?>
                <script type="text/javascript"> alert("Register success, please confirm your email")</script>
                <?php
            }
        
           
        }

       
        

	}
}
else
{
	echo "No Access";
}

?>