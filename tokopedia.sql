-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Okt 2016 pada 14.31
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tokopedia`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `prefix_account`
--

CREATE TABLE `prefix_account` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` int(50) NOT NULL,
  `password` int(50) NOT NULL,
  `gender` int(1) NOT NULL,
  `datebirth` date NOT NULL,
  `date` datetime NOT NULL,
  `confirm` tinyint(1) NOT NULL,
  `publish` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `prefix_admin`
--

CREATE TABLE `prefix_admin` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `prefix_admin`
--

INSERT INTO `prefix_admin` (`id`, `name`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `prefix_slideshow`
--

CREATE TABLE `prefix_slideshow` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `img` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  `publish` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `prefix_slideshow`
--

INSERT INTO `prefix_slideshow` (`id`, `name`, `link`, `img`, `date`, `publish`) VALUES
(1, 'satu', '#', 'barang1.jpg.webp', '2016-10-07 00:00:00', 1),
(2, 'dua', '#', 'barang2.jpg.webp', '2016-10-08 00:00:00', 1),
(3, 'tiga', '#', 'barang3.jpg.webp', '2016-10-09 00:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `prefix_account`
--
ALTER TABLE `prefix_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prefix_admin`
--
ALTER TABLE `prefix_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prefix_slideshow`
--
ALTER TABLE `prefix_slideshow`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `prefix_account`
--
ALTER TABLE `prefix_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `prefix_admin`
--
ALTER TABLE `prefix_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `prefix_slideshow`
--
ALTER TABLE `prefix_slideshow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
