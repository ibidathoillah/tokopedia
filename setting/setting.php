<?php
if(empty($secure) or $secure!='OK'){
	header("location:http://$domain/error/");
}
else {
	class connection {
		public $conn;
		private $host = "localhost";
		private $user = "root";
		private $pass = "";
		private $dbname = "tokopedia";
		
		function get_connection () {
			$this->conn = mysqli_connect($this->host,$this->user,$this->pass,$this->dbname);
			return $this->conn;
		}
	}
	
	class config {
		public $domain = "localhost/tokopedia";
		public $domainadmin = "admin";
		public $tabel_prefix = "prefix_";
		
		function domain(){

			return $this->domain;
		}

		function domainadmin(){
			return $this->domain."/".$this->domainadmin;
		}
		
		function set_tabel ($tabel) {
			return ($this->tabel_prefix.$tabel);
		}
	}
}
?>