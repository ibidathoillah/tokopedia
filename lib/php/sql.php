<?php
if(empty($secure) or $secure!='OK'){
	header("location:http://$domain/error/");
}
else {
	class smsql {
		/* FUNGSI QUERY */
		public function query ($value){
			global $connect;
			$this->query=mysqli_query($connect->conn,$value);
		}
	
		/* FUNGSI ARRAY */
		public function fetch_array (){
			$this->data = mysqli_fetch_array($this->query);
			return $this->data;
		}
		
		/* FUNGSI NUM ROWS*/
		public function num_rows () {
			$this->num_rows = mysqli_num_rows ($this->query);
			return $this->num_rows;
		}
		
		public function close () {
			mysqli_close($this->query);	
		}

		/* FUNGSI GET VALUE (NAMA TABEL, KONDISI, HASIL YG DIKEMBALIKAN)
			MENDAPATKAN HASIL DENGAN KONDISI TERTENTU */
		public function get_value ($tabel,$kondisi,$return){
			global $connect;
			if($kondisi==""){ $kondisi = ""; } else { $kondisi = "WHERE $kondisi"; }
			$config = new config(); $tabel = $config -> set_tabel($tabel);
			$query = mysqli_query($connect->conn,"SELECT $return FROM $tabel $kondisi");
			$data = mysqli_fetch_array ($query);
			return $data[$return];
		}
		
		/* FUNGSI QUERY VALUE (CUSTOM KONDISI, HASIL YG DIKEMBALIKAN)
			MENDAPATKAN HASIL DENGAN KONDISI TERTENTU */
		public function query_value ($kondisi,$return){
			global $connect;
			$query = mysqli_query($connect->conn,$kondisi);
			$data = mysqli_fetch_array ($query);
			return $data[$return];
		}
		/*
		FOR INSERT
		*/
		public function insert ($tabel,$data,$kondisi){
			global $connect;
			if($kondisi==""){ $kondisi = ""; } else { $kondisi = "WHERE $kondisi"; }
			$config = new config(); $tabel = $config -> set_tabel($tabel);
			$query_insert = "INSERT INTO $tabel VALUES (".$data.") $kondisi";
			$query = mysqli_query($connect->conn,$query_insert);
			
			return $query;
		}
		/*
		FOR UPDATE
		*/
		public function update ($tabel,$data,$kondisi){
			global $connect;
			if($kondisi==""){ $kondisi = ""; } else { $kondisi = "WHERE $kondisi"; }
			$config = new config(); $tabel = $config -> set_tabel($tabel);
			$query_update = "UPDATE $tabel SET $data $kondisi";
			$query = mysqli_query($connect->conn,$query_update);
			return $query;
		}
		
	}

}
?>