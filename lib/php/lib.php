<?php
/**
* 
*/
class page
{


	function not_found()
	{
		global $domain;
		$this-> get_head("404 Not Found","Not Found");
		?>
		<div class="container mt-20"><div class="row-fluid"><div class="span6"><h1 class="mb-0">Halaman Tidak Ditemukan</h1><p>Maaf, halaman yang anda tuju tidak ditemukan (<span class="muted">error 404</span>).</p><div class="mt-30"><h2 class="fs-14 mb-0">Apa yang dapat Anda lakukan selanjutnya ?</h2><ol><li>Periksa kembali apakah alamat URL yang Anda masukkan sudah benar.</li><li>Anda dapat kembali ke halaman <a href="http://<?php echo $domain; ?>/">Beranda Tokopedia</a>.</li><li>Gunakan kolom pencarian (search) di bagian atas halaman Tokopedia untuk mencari produk yang Anda inginkan.</li><li>Untuk informasi lebih lanjut silakan kunjungi halaman <a href="http://<?php echo $domain; ?>/contact.pl" title="Pusat bantuan Tokopedia">pusat bantuan</a> kami.</li></ol></div></div><div class="span6"><div><img src="http://<?php echo $domain; ?>/lib/image/error-404-5.png" alt="Page Not Found"></div></div></div></div>
		<?php
		
	}


	public $title ="Jual Beli Online Aman dan Nyaman";
	public $description="Mal online terbesar Indonesia, tempat berkumpulnya toko / online shop terpercaya se Indonesia. Jual beli online semakin aman dan nyaman di Tokopedia.";
	public $icon = "favicon.ico";
	public $something="";

	function get_domain(){
		global $domain;
		return $domain;
	}

	function set_title($title){
		$this->title = $title;
	}

	function set_description($description){
		$this->description = $description;
	}

	function set_favicon(){
		$this->icon = $description;
	}

	function add($something)
	{
		$this->something .=$something;
	}

	function get_head(){
		
		global $domain;
		?>
		  <head>
		      <title><?php echo $this ->title; ?> | Tokopedia</title>
		      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		      <meta name="viewport" content="width=device-width, initial-scale=1.0">
		      <meta name="title" content="<?php echo $this ->title; ?> - Tokopedia" />
		      <meta name="description" content="<?php echo  $this ->description ?>" />
		      <link rel="shortcut icon" href="http://<?php echo $domain ?>/lib/img/<?php echo  $this ->icon ?>">
		      <link rel="stylesheet" href="http://<?php echo $domain ?>/lib/css/style.css" type="text/css"/>
		      <?php echo  $this ->something ?>
		   </head>
		<?php
	}
}

?>